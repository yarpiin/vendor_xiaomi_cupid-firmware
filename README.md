# vendor_xiaomi_cupid_firmware

Firmware images for Xiaomi 12 (cupid), to include in custom ROM builds.

**Current version**: fw_cupid_miui_CUPIDGlobal_V14.0.3.0.TLCMIXM

### How to use?

1. Clone this repo to `vendor/xiaomi/cupid/firmware`

2. Include it from `BoardConfig.mk` in device tree:

```
# Firmware
-include vendor/xiaomi/cupid/firmware/BoardConfigVendor.mk
```
